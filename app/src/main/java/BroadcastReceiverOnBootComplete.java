package br.com.filacerta.www.prototipofilacerta;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import br.com.filacerta.www.prototipofilacerta.ServicoGps;

/**
 * Created by adria on 15/02/2018.
 */

public class BroadcastReceiverOnBootComplete extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            Intent serviceIntent = new Intent(context, ServicoGps.class);
            context.startService(serviceIntent);
        }
    }
}
