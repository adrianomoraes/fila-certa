package br.com.filacerta.www.prototipofilacerta;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

public class logado extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "BOOMBOOMTESTGPS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logado);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Spinner estados = (Spinner) findViewById(R.id.spinnerEstado);
        ArrayAdapter<CharSequence> estadosadapter = ArrayAdapter.createFromResource(
                this, R.array.acoes, R.layout.spinner_layout);
        estadosadapter.setDropDownViewResource(R.layout.spinner_layout);
        estados.setAdapter(estadosadapter);


        Intent intent = new Intent(this, ServicoGps.class);
        startService(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        String str_result = "-";


        Log.e(TAG, "onStart: " + "INICIADO");

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.logado, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onClickEnviaEstadoFila(View view){
        String str_result = "-";
        Spinner estado = (Spinner) findViewById(R.id.spinnerEstado);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        SimpleDateFormat formatacao = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Log.e(TAG, "onClickEnviaEstadoFila: " + formatacao.format(calendar.getTime()));

        //enviaEstado(
        try {
             str_result = new networkPush().execute(String.valueOf(estado.getSelectedItemId()),
                    formatacao.format(calendar.getTime()),
                    "1",//adriano.moraes@gmail.com
                    "2",//id_local
                    "",//4
                    "",//5
                    "",//6
                    "8",//7
                    /*URL*/ "http://www.filacerta.com.br/cgi/update_state.py",
                    "id_estado",
                    "datahora_cliente",
                    "id_usuario",
                    "id_local",
                    "14",
                    "15",
                    "16",
                    "17",
                    "logado.onClickEnviaEstadoFila").get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        JSONObject json = null;
        try {
            //);

            JSONParser parser = new JSONParser();
            Object obj  = parser.parse(str_result);
            JSONArray array = new JSONArray();
            array.put(obj);

            JSONObject jsnobject = new JSONObject(str_result);

            String resultState = jsnobject.getString("state");


            Log.e(TAG, "onClickEnviaEstadoFila - resultData: " + resultState);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
