package br.com.filacerta.www.prototipofilacerta;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by adria on 12/02/2018.
 */

public class networkPush extends AsyncTask<String,Void, String> {
    private static final String TAG = "BOOMBOOMTESTGPS";
    private String chamador = "";

    InputStream inputStream = null;
    String result = "";

    /** The system calls this to perform work in a worker thread and
     * delivers it the parameters given to AsyncTask.execute() */
    protected String doInBackground(String... vars) {
        Log.e(TAG, "ondoInBackground: " + vars[8]);
        chamador = vars[17];

        RequestBody formBody = new FormBody.Builder()
                .add(vars[9], vars[0])
                .add(vars[10], vars[1])
                .add(vars[11], vars[2])
                .add(vars[12], vars[3])
                .add(vars[13], vars[4])
                .add(vars[14], vars[5])
                .add(vars[15], vars[6])
                .add(vars[16], vars[7])
                .build();
        Request request = new Request.Builder()
                .url(vars[8])
                .post(formBody)
                .build();
        OkHttpClient client = new OkHttpClient();

        try {

            Response response = client.newCall(request).execute();
            //Log.e(TAG, "ondoInBackground: responseCode = " +String.valueOf(response.code()));

            if (response.code() == 200) {
                chamador = vars[17];
                //Log.e(TAG, "ondoInBackground: responseBody = " + response.peekBody(1000000).string());
                String responseData = response.body().string();

                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    inputStream = new ByteArrayInputStream(responseData.getBytes(StandardCharsets.UTF_8));
                }
                Log.e(TAG, "ondoInBackground: responseCode = " + inputStream.toString());

                //Process the response Data
                /*try {
                    BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
                    StringBuilder sBuilder = new StringBuilder();

                    String line = null;
                    while ((line = bReader.readLine()) != null) {
                        sBuilder.append(line + "\n");
                    }

                    inputStream.close();
                    result = sBuilder.toString();

                } catch (Exception e) {
                    Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
                }*/

                return responseData;
            } else {
                //Server problem
                return String.valueOf(response.code());
            }
        } catch (IOException e) {
            Log.e(TAG, "ondoInBackground: falhou");
            e.printStackTrace();
            return "falhou";
        }



        //return "fail try";
    }

    /** The system calls this to perform work in the UI thread and delivers
     * the result from doInBackground() */
    protected void onPostExecute(Void v) {
        Log.e(TAG, "onNetworkPush: " + result.toString());

        if (chamador == "logado.onStart") {
            //try {
                //JSONArray jArray = new JSONArray(result);
                //for (int i = 0; i < jArray.length(); i++) {

                  //  JSONObject jObject = jArray.getJSONObject(i);

                /*String name = jObject.getString("name");
                String tab1_text = jObject.getString("tab1_text");
                int active = jObject.getInt("active");*/
                    //Log.e(TAG, "onNetworkPush-onPostExecute: " + jObject.toString());


                //} // End Loop
            /*this.progressDialog.dismiss();*/

            //} catch (JSONException e) {
            //    Log.e("JSONException", "Error: " + e.toString());
            //} // catch (JSONException e)
        }

    }
}
